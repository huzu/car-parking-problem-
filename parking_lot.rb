require_relative 'lib/parking_service.rb'
require 'active_support/all'


class ParkingLot
  # this method processed the input given in the command prompt. 
  def self.process_file_input(input)
    latest_parking_lot = nil
    input.split("\n").each do |input|
      sliced_input = input.split(/\s/)
      command = sliced_input[0]
      if command == 'create_parking_lot'
        no_of_slots = sliced_input[1]
        latest_parking_lot = ParkingService.new(no_of_slots)
      else        
        latest_parking_lot.send(command, *([sliced_input[1], sliced_input[2]].compact))
      end
    end
  end

  def self.process_cmd_input    
    commands_defined = ["create_parking_lot", "park", "status", "leave", "registration_numbers_for_cars_with_colour", "slot_numbers_for_cars_with_colour", "slot_number_for_registration_number", "exit"]    
    to_continue = true
    
    while to_continue
      shell_input = gets()
      string_to_arr = shell_input.split(" ")
      command = string_to_arr.first

      if validate_command(commands_defined, command)
        case command
        when 'create_parking_lot'
          slot_details = string_to_arr.last
          no_of_slots = slot_details
          latest_parking_lot = ParkingService.new(no_of_slots)
        when 'exit'
          to_continue = false
          break
        else
          string_to_arr = shell_input.split(" ")
          latest_parking_lot.send(command, *([string_to_arr[1], string_to_arr[2]].compact))
        end
      else
        puts "please enter a valid command"
      end
    end
  end

  def self.validate_command(commands = [], user_input_command)
    commands.select{|c| c == user_input_command}.present?
  end  
end