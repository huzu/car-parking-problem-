require_relative '../lib/vehicle_details'

describe VehicleDetails do
  describe 'initialize' do
    context 'given input paramaters is correct' do
      it 'returns new vehicle instance' do
        vehicle = VehicleDetails.new('number', 'color')
        expect(vehicle.class).to eql(VehicleDetails)
        expect(vehicle.vehicle_number).to eql('number')
        expect(vehicle.vehicle_color).to eql('color')
      end
    end

    context 'given input paramaters have partial or no parameters' do
      it 'throws exception due to missing argument errors' do
        expect { VehicleDetails.new }.to raise_error(ArgumentError)
        expect { VehicleDetails.new('number') }.to raise_error(ArgumentError)
        expect { VehicleDetails.new('color') }.to raise_error(ArgumentError)
      end
    end
  end
end